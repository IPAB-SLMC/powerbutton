#include "libpowerbutton.h"

int power_button_get_value()
{
    using namespace boost::interprocess;
    shared_memory_object segment( open_or_create, "PowerButtonMemory", read_write);
    segment.truncate(sizeof(int));
    offset_t size;
    if(segment.get_size(size))
    {
        mapped_region region(segment, read_write);
        int* val = static_cast<int*>(region.get_address());
        return *val;
    }
    else
        return 0;
}

void power_button_reset()
{
    using namespace boost::interprocess;
    shared_memory_object segment( open_or_create, "PowerButtonMemory", read_write);
    segment.truncate(sizeof(int));
    offset_t size;
    if(segment.get_size(size))
    {
        mapped_region region(segment, read_write);
        int* val = static_cast<int*>(region.get_address());
        *val = 0;
    }
}

