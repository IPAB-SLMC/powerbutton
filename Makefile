ARCH = $(shell uname -m)
ifeq ($(ARCH),x86_64)
CXXFLAGS = -fPIC
CFLAGS = -fPIC
LDFLAGS = -fPIC
endif

prefix=/usr/local

all: powerbutton.cpp libpowerbutton.cpp
	$(CXX) powerbutton.cpp -o powerbutton -I/usr/local/include -lpthread -lrt $(CXXFLAGS) $(CFLAGS) $(LDFLAGS)
	$(CXX) -shared libpowerbutton.cpp -o libpowerbutton.so -I/usr/local/include -lpthread -lrt $(CXXFLAGS) $(CFLAGS) $(LDFLAGS)

install: libpowerbutton.so
	install libpowerbutton.so $(prefix)/lib
	mkdir $(prefix)/include/powerbutton 
	install libpowerbutton.h $(prefix)/include/powerbutton/