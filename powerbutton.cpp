#define BOOST_DATE_TIME_NO_LIB
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <sys/stat.h>
#include <fcntl.h>

int main (int argc, char *argv[])
{
    using namespace boost::interprocess;
    shared_memory_object segment( open_or_create, "PowerButtonMemory", read_write);
    int fildes;
    fildes = open("/dev/shm/PowerButtonMemory", O_RDWR);
    fchmod(fildes, S_IRWXU | S_IRWXG | S_IROTH | S_IWOTH);
    close(fildes);
    segment.truncate(sizeof(int));
    offset_t size;
    if(segment.get_size(size))
    {
        mapped_region region(segment, read_write);
        int* val = static_cast<int*>(region.get_address());
        if(argc>1)
        {
            if(argv[1][0]=='0')
            {
                *val = 0;
                printf("Power button interface activated\n",*val);
            }
            else
            {
                printf("Power button pressed: %i\n",*val);
            }
        }
        else
        {
            *val = *val+1; 
            printf("Power button pressed: %i\n",*val);
        }
    }
};
